function multiply(){
    var number1 = document.getElementById("number1"); //Get element number 1 by id of element
    var number2 = document.getElementById("number2"); //Get element number 2 by id of element
    var error = document.getElementById("error"); //Get element error by id of element
    var result = document.getElementById("result"); //Get element result by id of element
    
    if(isNaN(number1.value) || isNaN(number2.value)){
		error.innerHTML = "Inserte números válidos";
	} else {
		error.innerHTML = "";
		result.innerHTML = number1.value * number2.value;
    }	
    

}