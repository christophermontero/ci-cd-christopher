function diff() {
    var number1 = document.getElementById("number1").value;
    var number2 = document.getElementById("number2").value;
    var error = document.getElementById("error");
    var diff = number1 - number2;

    if (isNaN(number1) || isNaN(number2)) {
        error.innerHTML = "Inserte numeros válidos";
    } else {
        error.innerHTML = "";
        document.getElementById("result").innerHTML = diff;
    }
}