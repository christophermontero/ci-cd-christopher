function divide(){
    var number1 = document.getElementById("number1").value; //Get element number 1 by id of element
    var number2 = document.getElementById("number2").value; //Get element number 2 by id of element
    var error = document.getElementById("error"); //Get element error by id of element
    var result = document.getElementById("result"); //Get element result by id of element

    if(isNaN(number1) || isNaN(number2)){
		error.innerHTML = "Inserte numeros validos";
	} else {
        
        if(number2 == 0){
            error.innerHTML = "No puede divir por cero!";
            return;
        }
        result.innerHTML = number1 / number2;
		error.innerHTML = "";
    }


}